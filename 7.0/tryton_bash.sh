#!/bin/sh

set -a
source ./.env

if [ ! "$(docker ps -q -f name=^/tryton_bash_70$)" ]; then
    if [ "$(docker ps -aq -f status=exited -f name=^/tryton_bash_70$)" ]; then
        # cleanup
        docker rm tryton_bash_70
    fi
    workpath="/home/trytond/tryton"
    if [ "${1}" == "root" ] || [ "${1}" == "0" ]; then
        workpath="/"
    fi
    # run your container
    docker run \
        --rm \
        -u ${1:-trytond} \
        -w=$workpath \
        -v "docker-tryton_code_70:/home/trytond/tryton" \
        -v "docker-tryton_db_70:/home/trytond/db" \
        --link postgres \
        --network=docker-tryton_default \
        -e DB_HOSTNAME=postgres \
        -e DB_USER=${TRYTON_DB_USER} \
        -e DB_PASSWORD=${TRYTON_DB_PASSWORD} \
        -e TRYTOND_DATABASE__LANGUAGE=${TRYTON_LANGUAGE} \
        --name tryton_bash_7 \
        -it tryton:7.0-deploy bash
fi
