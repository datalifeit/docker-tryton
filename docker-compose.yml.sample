version: '3.8'

x-logging:
  &default-logging
  options:
    max-size: "5m"
    max-file: "3"

services:
  postgres:
    image: postgres:${POSTGRES_VERSION}
# use alpine image if fails access to volume 'docker-entrypoint-initdb.d'
#    image: postgres:${POSTGRES_VERSION}-alpine
    container_name: postgres
    restart:
      always
    environment:
      - POSTGRES_PASSWORD=${POSTGRES_PASSWORD}
      - TRYTON_DB_USER=${TRYTON_DB_USER}
      - TRYTON_DB_PASSWORD=${TRYTON_DB_PASSWORD}
      - TRYTON_DB_NAME=${TRYTON_DB_NAME}
    volumes:
      - ./sql_init_scripts:/docker-entrypoint-initdb.d
      - /var/lib/pgsql/${POSTGRES_VERSION}/data:/var/lib/postgresql/data
    ports:
      - "5432:5432"
    shm_size: '128mb'
    logging: *default-logging

  tryton:
    build: ${VERSION}/.
    image: tryton:${VERSION}-deploy
    container_name: tryton
    environment:
      DB_USER: ${TRYTON_DB_USER}
      DB_PASSWORD: ${TRYTON_DB_PASSWORD}
      DB_HOSTNAME: postgres
      TRYTOND_DATABASE__LANGUAGE: ${TRYTON_LANGUAGE}
      TRYTOND_LOGGING_LEVEL: ${TRYTOND_LOGGING_LEVEL}
# <<<< comment to use trytond without uwsgi
    expose:
      - "8000"
# >>>> finish comment

# <<<< uncomment to use trytond without uwsgi
#    ports:
#      - "8000:8000"
#    entrypoint:
#      /entrypoint.sh trytond
# >>>> finish uncomment
    depends_on:
      - postgres
    volumes:
      - db:/home/trytond/db
      - code:/home/trytond/tryton
    restart:
      unless-stopped
    logging: *default-logging

  nginx:
    image: nginx:latest
    container_name: nginx
    volumes:
      - ./nginx.conf:/etc/nginx/nginx.conf
# <<<< uncomment to use ssl and certbot
#      - ./certbot/conf:/etc/letsencrypt
#      - ./certbot/www:/var/www/certbot
#    command: "/bin/sh -c 'while :; do sleep 6h & wait $${!}; nginx -s reload; done & nginx -g \"daemon off;\"'"
# >>>> finish uncomment
    ports:
      - 80:80
      - 8000:8000
# <<<< uncomment to use ssl and certbot >>>>
#     - 443:443
    depends_on:
      - tryton
    restart:
      unless-stopped
    logging: *default-logging

# <<<< uncomment to use ssl and certbot
#  certbot:
#    image: certbot/certbot
#    volumes:
#      - ./certbot/conf:/etc/letsencrypt
#      - ./certbot/www:/var/www/certbot
#    entrypoint: "/bin/sh -c 'trap exit TERM; while :; do certbot renew; sleep 12h & wait $${!}; done;'"
# >>>> finish uncomment

  tryton-cron:
    build: ${VERSION}/.
    image: tryton:${VERSION}-deploy
    container_name: tryton-cron
    environment:
      DB_USER: ${TRYTON_DB_USER}
      DB_PASSWORD: ${TRYTON_DB_PASSWORD}
      DB_HOSTNAME: postgres
      TRYTOND_DATABASE__LANGUAGE: ${TRYTON_LANGUAGE}
      TRYTOND_LOGGING_LEVEL: ${TRYTOND_LOGGING_LEVEL}
    depends_on:
      - postgres
    entrypoint:
      /entrypoint.sh trytond-cron -d ${TRYTON_DB_NAME}
    volumes:
      - db:/home/trytond/db
      - code:/home/trytond/tryton
    restart:
      unless-stopped
    logging: *default-logging

  tryton-worker:
    build: ${VERSION}/.
    image: tryton:${VERSION}-deploy
    container_name: tryton-worker
    environment:
      DB_USER: ${TRYTON_DB_USER}
      DB_PASSWORD: ${TRYTON_DB_PASSWORD}
      DB_HOSTNAME: postgres
      TRYTOND_DATABASE__LANGUAGE: ${TRYTON_LANGUAGE}
      TRYTOND_LOGGING_LEVEL: ${TRYTOND_LOGGING_LEVEL}
    depends_on:
      - postgres
    entrypoint:
      /entrypoint.sh trytond-worker -d ${TRYTON_DB_NAME}
    volumes:
      - db:/home/trytond/db
      - code:/home/trytond/tryton
    restart:
      unless-stopped
    logging: *default-logging

volumes:
    db:
    code: